;;; evil-latex-textobjects.el --- LaTeX text objects for evil

;; Copyright (C) 2015  Hans-Peter Deifel

;; Author: Hans-Peter Deifel <hpd@hpdeifel.de>
;; Keywords: tex, wp, convenience, vi, evil
;; Package-Version: 1.0.1
;; Version: 1.0.1
;; Package-Requires: ((evil "1.0"))

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;;
;;; Provides a minor mode that installs several additional LaTeX
;;; specific text objects for evil-mode:
;;;
;;;  \	Display math		\[ .. \]
;;;  $	Inline math		$ .. $
;;;  m	TeX macro		\foo{..}
;;;  e	LaTeX environment	\begin{foo}..\end{foo}
;;;
;;; To enable this mode in LaTeX buffers, add this to your init file:
;;;
;;; (require 'evil-latex-textobjects)
;;; (add-hook 'LaTeX-mode-hook 'turn-on-evil-latex-textobjects-mode)
;;;
;;; or
;;;
;;; (add-hook 'evil-local-mode-hook 'check-and-turn-on-evil-latex-textobjects-mode)

;;; Code:

(require 'evil)

(evil-define-text-object evil-latex-textobjects-inner-dollar (count &optional beg end type)
  "Select inner dollar"
  :extend-selection nil
  (evil-select-quote ?$ beg end type count nil))

(evil-define-text-object evil-latex-textobjects-a-dollar (count &optional beg end type)
  "Select a dollar"
  :extend-selection t
  (evil-select-quote ?$ beg end type count t))

(evil-define-text-object evil-latex-textobjects-inner-math (count &optional beg end type)
  "Select innter \\[ \\] or \\( \\)."
  :extend-selection nil
  (evil-select-paren "\\\\\\[\\|\\\\(" "\\\\)\\|\\\\\\]" beg end type count nil))

(evil-define-text-object evil-latex-textobjects-a-math (count &optional beg end type)
  "Select a \\[ \\] or \\( \\)."
  :extend-selection nil
  (evil-select-paren "\\\\\\[\\|\\\\(" "\\\\)\\|\\\\\\]" beg end type count t))

(defun evil-latex-textobjects-macro-beginning ()
  "Return (start . end) of the macro-beginning to the left of point.

If no enclosing macro is found, return nil.
For example for \macro{foo|bar} it returns the start and end of \"\macro{\""
  (let ((beg (re-search-backward "\\\\[^{]+{")))
    (when beg
      (save-excursion
        (goto-char beg)
        (forward-char)                  ; backslash
        (skip-chars-forward "A-Za-z@*") ; macro-name
        (when (looking-at "{\\|\\[")
          (forward-char))                ; opening brace
        (cons beg (point))))))

(defun evil-latex-textobjects-macro-end ()
  "Return (start . end) of the end of the enclosing macro.

If no such macro can be found, return nil"
  (let ((end (re-search-forward "}")))
    (when end
      (save-excursion
        (goto-char end)
        (when (looking-back "}\\|\\]")
          (backward-char))               ; closing brace
        (cons (point) end)))))

;; TODO Support visual selection
;; TODO Support count

(evil-define-text-object evil-latex-textobjects-a-macro (count &optional beg end type)
  "Select a TeX macro"
  :extend-selection nil
  (let ((beg (evil-latex-textobjects-macro-beginning))
        (end (evil-latex-textobjects-macro-end)))
    (if (and beg end)
        (list (car beg) (cdr end))
      (error "No enclosing macro found"))))

(evil-define-text-object evil-latex-textobjects-inner-macro (count &optional beg end type)
  "Select inner TeX macro"
  :extend-selection nil
  (let ((beg (evil-latex-textobjects-macro-beginning))
        (end (evil-latex-textobjects-macro-end)))
    (cond
     ((or (null beg) (null end))
      (error "No enclosing macro found"))
     ((= (cdr beg) (car end))           ; macro has no content
      (list (1+ (car beg))              ; return macro boundaries excluding \
            (cdr beg)))
     (t (list (cdr beg) (car end))))))

(defun evil-latex-textobjects-env-beginning ()
  "Return (start . end) of the \\begin{foo} to the left of point."
  (let (beg)
    (save-excursion
      (LaTeX-find-matching-begin)       ; we are at backslash
      (setq beg (point))
      (skip-chars-forward "^{")         ; goto opening brace
      (forward-sexp)                    ; goto closing brace
      ;; Count the newline after \begin{foo} to the environment header
      ;; Without this, delete-inner-env would unexpectedly move the end
      ;; to the same line as the beginning
      ;; (when (looking-at "[[:blank:]]*$")
      ;;   (message "Newline")
      ;;   (forward-line 1))
      (cons beg (point)))))

(defun evil-latex-textobjects-env-end ()
  "Return (start . end) of the \\end{foo} to the right of point."
  (let (end)
    (save-excursion
      (LaTeX-find-matching-end)         ; we are at closing brace
      (setq end (point))
      (backward-sexp)                   ; goto opening brace
      (search-backward "\\")            ; goto backslash
      (cons (point) end))))


(evil-define-text-object evil-latex-textobjects-an-env (count &optional beg end type)
  "Select a LaTeX environment"
  :extend-selection nil
  (let* ((pair (evil-latex-textobjects-env-pos count))
	 (beg (car pair))
	 (end (cdr pair)))
    (list (car beg) (cdr end))))

(evil-define-text-object evil-latex-textobjects-inner-env (count &optional beg end type)
  "Select a LaTeX environment"
  :extend-selection nil
  (let* ((pair (evil-latex-textobjects-env-pos count))
	 (beg (car pair))
	 (end (cdr pair)))
    (list (cdr beg) (car end))))

(defvar evil-latex-textobjects-outer-map (make-sparse-keymap))
(defvar evil-latex-textobjects-inner-map (make-sparse-keymap))

(set-keymap-parent evil-latex-textobjects-outer-map evil-outer-text-objects-map)
(set-keymap-parent evil-latex-textobjects-inner-map evil-inner-text-objects-map)

(define-key evil-latex-textobjects-inner-map "$" 'evil-latex-textobjects-inner-dollar)
(define-key evil-latex-textobjects-outer-map "$" 'evil-latex-textobjects-a-dollar)
(define-key evil-latex-textobjects-inner-map "\\" 'evil-latex-textobjects-inner-math)
(define-key evil-latex-textobjects-outer-map "\\" 'evil-latex-textobjects-a-math)
(define-key evil-latex-textobjects-outer-map "m" 'evil-latex-textobjects-a-macro)
(define-key evil-latex-textobjects-inner-map "m" 'evil-latex-textobjects-inner-macro)
(define-key evil-latex-textobjects-outer-map "e" 'evil-latex-textobjects-an-env)
(define-key evil-latex-textobjects-inner-map "e" 'evil-latex-textobjects-inner-env)

;;;###autoload
(define-minor-mode evil-latex-textobjects-mode
  "Minor mode for latex-specific text objects in evil.

Installs the following additional text objects:
\\<evil-latex-textobjects-outer-map>
  \\[evil-latex-textobjects-a-math]\tDisplay math\t\t\\=\\[ .. \\=\\]
  \\[evil-latex-textobjects-a-dollar]\tInline math\t\t$ .. $
  \\[evil-latex-textobjects-a-macro]\tTeX macro\t\t\\foo{..}
  \\[evil-latex-textobjects-an-env]\tLaTeX environment\t\\begin{foo}..\\end{foo}"
  :keymap (make-sparse-keymap)
  (evil-normalize-keymaps))

(evil-define-key 'operator evil-latex-textobjects-mode-map
  "a" evil-latex-textobjects-outer-map
  "i" evil-latex-textobjects-inner-map)

(evil-define-key 'visual evil-latex-textobjects-mode-map
  "a" evil-latex-textobjects-outer-map
  "i" evil-latex-textobjects-inner-map)

;;;###autoload
(defun turn-on-evil-latex-textobjects-mode ()
  "Enable evil-latex-textobjects-mode in current buffer."
  (interactive "")
  (evil-latex-textobjects-mode 1))

;;;###autoload
(defun turn-off-evil-latex-textobjects-mode ()
  "Disable evil-latex-textobjects-mode in current buffer."
  (interactive "")
  (evil-latex-textobjects-mode -1))

;;;###autoload
(defun check-and-turn-on-evil-latex-textobjects-mode ()
  "Turn evil-latex-textobjects-mode on if major mode is latex-mode."
  (if (eq major-mode 'latex-mode)
      (turn-on-evil-latex-textobjects-mode)))

(defun evil-latex-textobjects-env-pos (&optional count)
  "Return ((start . end) . (start . end)) of the most inner surrounding environment of the point."
  (let (beg end)
    (save-excursion
      (when (evil-visual-state-p)	(goto-char (evil-range-beginning (evil-visual-range))))
      (when (null count) (setq count 1))
      (while (< 0 count)
	(texcomp:surrounding-environment)
	(setq count (- count 1)))
      (setq beg (point))
      (skip-chars-forward "^{")         ; goto opening brace
      (search-forward "}")              ; goto closing brace
      (setq beg (cons beg (point)))
      (goto-char (car beg))
      (forward-sexp)                   ; goto the corresponding end
      (setq end (point))
      (search-backward "\\")            ; goto backslash
      (setq end (cons (point) end))
      (cons beg end))))

(defun texcomp:surrounding-environment ()
  "FIXME: Close the current most-inner environment."
  (interactive "*")
  (let ((level 1)			; depth of environment
	(here (copy-marker (point-marker))) ; position to insert string
	(namelist nil)			; push down list of env. name
	(name "")			; name of most-inner env.
	temp)				; regexp matched ?
    (while (< 0 level)
      (setq temp (re-search-backward
		  (concat "\\\\\\(begin\\|end\\){\\([^}]+\\)}")
		  1 t))
      (if temp
	  (setq name
		(regexp-quote (buffer-substring (match-beginning 2) (match-end 2)))))
      (cond ((null temp)
	     (setq level (- level)))
	    ((eq (char-after (match-beginning 1)) ?b)
	     (if (or (string= name (car namelist)) (null namelist))
		 (setq namelist (cdr namelist))
	       (error "Unmatched environment:%s" name))
	     (setq level (1- level)))
	    ((eq (char-after (match-beginning 1)) ?e)
	     (setq namelist (cons name namelist))
	     (setq level (1+ level)))
	    (t
	     (setq level -1))))
    ;; Now level must be either 0 or minus number.
    ;; 0 means the environment was found. minus meams abnormally exit.
    (cond ((zerop level) (goto-char temp))
	  ((= level -1) (error "I think all environment are closed, isn't it ?"))
	  (t (error "Mismatched envrionment")))))

(defun texcomp:scan-environment (from count)
  "Scan from character number FROM by COUNT environments' terminator.
Return the cons of character numbers of the position thus found.
The car of the cons is the starting point of matched string. The cdr is
the end of it.

If DEPTH is nonzero, environment depth begins counting from that value,
only places where the depth in environment becomes zero are candidates for
stopping; COUNT such places are counted. Thus, a positive value for DEPTH
means go out levels.

If the begginning or end of (the visible part of) the buffer is readched
and the depth is wrong, an error is signaled.
If the depth is right but the count is not used up, nil is returned."
  (let ((inc (if (< 0 count) 1 -1))	; increment value
	(env-starter (if (< 0 count) ?b ?e))
	(env-terminator (if (< 0 count) ?e ?b))
	(namelist nil)			; push down list of env. name
	(name "")			; name of most-inner env.
	temp				; regexp matched ?
        (back-to-same-level nil)	; loop exit flag
	not-reached)			; exception flag
    (while (/= count 0)
      (setq temp (funcall
		  (if (< 0 count) (function re-search-forward)
		    (function re-search-backward))
		  (concat "\\\\\\(begin\\|end\\){\\([^}]+\\)}")
		  (point-max) t))
      (if temp
	  (setq name (regexp-quote
		      (buffer-substring (match-beginning 2) (match-end 2)))))
      (cond ((null temp)
	     (if namelist
		 (error "Mismatched envrionment")
	       ;; Exception is occured.
	       (setq not-reached t
		     count 0)))
	    ((eq (char-after (match-beginning 1)) env-starter)
	     (cond ((and (null namelist) (= count inc))
		    (setq count (- count inc)))
		   (t
		    (setq namelist (cons name namelist)))))
	    ((eq (char-after (match-beginning 1)) env-terminator)
	     (cond ((string= name (car namelist))
		    (setq namelist (cdr namelist))
		    (setq count (- count inc)))
		   (t
		    (error "Unmatched environment: %s" name))))))
    (cond (not-reached nil)
	  ((< 0 inc) (match-end 0))
	  (t (match-beginning 0)))))

(provide 'evil-latex-textobjects)

;;; evil-latex-textobjects.el ends here
